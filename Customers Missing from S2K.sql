/*
	SELECT IsValidated
		, count(*)
	FROM [common].[CustomersAllVal]
	GROUP BY IsValidated
*/
WITH S2K_Cust as
	(
		SELECT *
		FROM [DW_Main].Sales.CustomerMapping
		WHERE SourceSystemSK = 1
	)
	, AX_Cust as
	(
		SELECT *
		FROM [DW_Main].Sales.CustomerMapping
		WHERE SourceSystemSK = 2
	)

SELECT a.CustomerSK
	, a.CustomerID
	, a.RelatedCustomerID
	, a.CustomerName
FROM AX_Cust a
	left join S2K_Cust s
		on a.CustomerSK = s.CustomerSK
WHERE s.CustomerSK is null