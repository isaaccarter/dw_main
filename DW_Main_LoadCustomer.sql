/*
--S2K Customers
With DupPossible as
	(
		SELECT min(CustomerNumber) CustomerNumber
			--, BillToName
			, left(BillToZip, 5) Zip
			, SOUNDEX(BillToName) SoundX
			--, COUNT(*)
		FROM S2K.Customers
		GROUP BY  left(BillToZip, 5)
			, SOUNDEX(BillToName) 
		--HAVING count(*) > 1
	)
	, CustomerList as
	(
		SELECT p.CustomerNumber as MatchCustomerNumber
			, c.CustomerNumber
			, c.BillToName
			, c.BillToAddress1
			, c.BillToAddress2
			, c.BillToCity
			, c.BillToCountry
			, c.BillToState
			, c.BillToZip
			, SOUNDEX(BillToName) SoundCode
			, 1 as SourceSystemSK
			
		FROM S2K.Customers c
			left join DupPossible p
				on left(c.BillToZip, 5) = p.Zip
				and SOUNDEX(BillToName) = p.SoundX
			left join DW_Main.Sales.CustomerMapping cm
				on c.CustomerNumber = cm.CustomerID
		--ORDER BY BillToName
	)

	MERGE [DW_Main].[Sales].[CustomerMapping] t
	USING CustomerList s
		on t.[CustomerID] = s.CustomerNumber
		and t.SourceSystemSK = s.SourceSystemSK

	When Matched then 
		update set t.[CustomerName] = s.BillToName
				, [UpdateDateKey] = format(cast(getdate() as date),'yyyyMMdd')
				, [SourceMatchCode] = left(BillToZip, 5)
	When Not Matched then
		insert ([SourceSystemSK], [CustomerID], [RelatedCustomerID], [CustomerName], [CreatedByEmployeeKey], [InsertDateKey],[SourceMatchCode] )
		values (s.[SourceSystemSK], s.CustomerNumber, s.MatchCustomerNumber, s.BillToName, 1, format(cast(getdate() as date),'yyyyMMdd'), left(BillToZip, 5))
	
	;
	--DELETE FROM  DW_Main.Sales.Customer

	--Now we update the CustomerSK
	WITH DistinctRelatedCustomers as
	(
		SELECT Distinct [RelatedCustomerID]
		FROM DW_Main.[Sales].[CustomerMapping]
		WHERE [CustomerSK] is null
	)
	--Stage the SKs
	SELECT [RelatedCustomerID]
		, Next Value For DW_Main.[Sales].[CustomerSK] CustomerSK
	INTO #TempCustomer
	FROM DistinctRelatedCustomers

	--Insert the Customer Record
	INSERT INTO DW_Main.Sales.Customer([CustomerSK], [SourceSystemSK], [CustomerName],[CreatedByEmployeeKey], [InsertDateKey])
	SELECT CustomerSK
		, 1
		, '~'
		, 1
		, format(cast(getdate() as date),'yyyyMMdd')
	FROM #TempCustomer

	--Update the CustomerSK in the mapping table
	UPDATE  m
		set  m.[CustomerSK] = d.CustomerSK
	FROM #TempCustomer d
		inner join [DW_Main].sales.[CustomerMapping] m
			on d.RelatedCustomerID = m.RelatedCustomerID

	Drop Table #TempCustomer;

	--Time to do AX

	--We can do the Matched Customers first
	WITH MatchedCustomers as
		(
			SELECT c.[AccountNum]
				, cm.CustomerSK
				, c.[BillToName]
				, c.BillToZip
				, 2 as SourceSystemSK
			FROM [AX].[Customers] c
				inner join DW_Main.Sales.CustomerMapping cm
					on c.CustomerNumber = cm.RelatedCustomerID
			--WHERE isnull([CustomerNumber], '') <> ''

		)
	/*
	No fuzzy matching here.  This is a direct lookup to already loaded customers
	*/
	MERGE [DW_Main].[Sales].[CustomerMapping] t
	USING MatchedCustomers s
		on t.[CustomerID] = s.[AccountNum]
		and t.SourceSystemSK = s.SourceSystemSK

	When Matched then 
		update set t.[CustomerName] = s.BillToName
				, [UpdateDateKey] = format(cast(getdate() as date),'yyyyMMdd')
				, [SourceMatchCode] = left(BillToZip, 5)
	When Not Matched then
		insert ([SourceSystemSK], [CustomerID], [RelatedCustomerID], [CustomerName], [CreatedByEmployeeKey], [InsertDateKey], [SourceMatchCode])
		values (s.[SourceSystemSK], s.[AccountNum],s.[AccountNum], s.BillToName, 1, format(cast(getdate() as date),'yyyyMMdd'), left(s.BillToZip, 5));


	/*
		now we do some fuzzy logic
	*/

	With AX_DupPossible as
	(
		SELECT min([AccountNum]) AccountNumber
			
			--, BillToName
			, left(BillToZip, 5) Zip
			, SOUNDEX(BillToName) SoundX
			--, COUNT(*)
		FROM AX.Customers
		GROUP BY  left(BillToZip, 5)
			, SOUNDEX(BillToName) 
		HAVING count(*) > 1
	)
	, CustomerList as
	(
		SELECT c.AccountNum as AccountNumber
			, case when p.AccountNumber is null then c.AccountNum else p.AccountNumber end as RelatedAccount
			, c.CustomerNumber
			, c.BillToName
			, c.BillToAddress1
			, c.BillToAddress2
			, c.BillToCity
			, c.BillToCountry
			, c.BillToState
			, c.BillToZip
			, SOUNDEX(BillToName) SoundCode
			, 1 as SourceSystemSK
			
		FROM AX.Customers c
			left join AX_DupPossible p
				on left(c.BillToZip, 5) = p.Zip
				and SOUNDEX(BillToName) = p.SoundX
			left join DW_Main.Sales.CustomerMapping cm
				on c.CustomerNumber = cm.CustomerID
		WHERE cm.CustomerID is null
		--ORDER BY BillToName
	)

	MERGE [DW_Main].[Sales].[CustomerMapping] t
	USING CustomerList s
		on t.[CustomerID] = s.AccountNumber
		and t.SourceSystemSK = s.SourceSystemSK

	When Matched then 
		update set t.[CustomerName] = s.BillToName
				, [UpdateDateKey] = format(cast(getdate() as date),'yyyyMMdd')
				, [SourceMatchCode] = left(BillToZip, 5)
	When Not Matched then
		insert ([SourceSystemSK], [CustomerID], [RelatedCustomerID], [CustomerName], [CreatedByEmployeeKey], [InsertDateKey], [SourceMatchCode])
		values (s.[SourceSystemSK], s.AccountNumber,s.RelatedAccount, s.BillToName, 1, format(cast(getdate() as date),'yyyyMMdd'), left(s.BillToZip, 5));
*/


	--Now we update the CustomerSK
	WITH DistinctRelatedCustomers as
	(
		SELECT Distinct [RelatedCustomerID]
		FROM DW_Main.[Sales].[CustomerMapping]
		WHERE [CustomerSK] is null
	)
	--Stage the SKs
	SELECT [RelatedCustomerID]
		, Next Value For DW_Main.[Sales].[CustomerSK] CustomerSK
	INTO #TempCustomer
	FROM DistinctRelatedCustomers

	--Insert the Customer Record
	INSERT INTO DW_Main.Sales.Customer([CustomerSK], [SourceSystemSK], [CustomerName],[CreatedByEmployeeKey], [InsertDateKey])
	SELECT CustomerSK
		, 2 --AX
		, '~'
		, 1
		, format(cast(getdate() as date),'yyyyMMdd')
	FROM #TempCustomer

	--Update the CustomerSK in the mapping table
	UPDATE  m
		set  m.[CustomerSK] = d.CustomerSK
	FROM #TempCustomer d
		inner join [DW_Main].sales.[CustomerMapping] m
			on d.RelatedCustomerID = m.RelatedCustomerID

	Drop Table #TempCustomer;


	/*

--AX Customer Time
--We will get the Customers that have set ID's first
SELECT c.CustomerNumber
	, cm.CustomerID
	, c.BillToName
	, cm.CustomerName
	, c.BillToZip
	, [dbo].[fConfidence](c.BillToName, cm.CustomerName)
FROM AX.Customers c
	left join DW_Main.Sales.CustomerMapping cm
		on left(c.BillToZip, 5) = cm.[SourceMatchCode]
		and [dbo].[fConfidence](c.BillToName, cm.CustomerName) > .85
 

 */
