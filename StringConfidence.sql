


DECLARE @str1 varchar(max) = 'Isaac'
	, @str2 varchar(max) = 'Isac'
	, @Levecshtein int 
	, @Difference int
	, @AvgStringLength float
	, @Confidence float
	, @FinalConfidence float
--Set the parameters
SELECT @Levecshtein = [dbo].[Levenshtein](@str1, @str2, 4000)
	, @Difference = difference(@str1, @str2)--this performs a 
	, @AvgStringLength = cast(len(@str1) + len(@str2) as float) /2



		set @confidence = 1 - (
								@Levecshtein / case when @AvgStringLength >=4 then @AvgStringLength + @Difference --If the string is at least 5 characters we will offset the length based on the soundex.  If the strings sound simular we can increase the confidence by up to 8%
													else @AvgStringLength end --don't offset if string length is less than 5
							 )

set @FinalConfidence = case when @confidence < .3 then 0 else @confidence end 

SELECT @FinalConfidence as Confidence
